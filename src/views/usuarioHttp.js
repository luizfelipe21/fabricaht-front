const API_URL = "http://localhost:3000/usuarios"

import axios from "axios"

export default {
    async buscarTodos(){
        return axios.get(API_URL).then((response) => response.data)
    },

    async adicionar(user){
        return axios.post(API_URL, user).then((response) => response)
    },

    async editar(user){
        return axios.put(API_URL, user).then((response) => response)
    },

}